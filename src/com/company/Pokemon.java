package com.company;

public class Pokemon {
    private String nombre;
    private String tipo;
    private String sonido;
    private String ataque;
    private double peso;
    private double altura;

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSonido() {
        return sonido;
    }

    public void setSonido(String sonido) {
        this.sonido = sonido;
    }

    public String getAtaque() {
        return ataque;
    }

    public void setAtaque(String ataque) {
        this.ataque = ataque;
    }

    public void habla(){
        System.out.println(this.sonido);
    }

    void ataca(String otroPokemon){
        System.out.println(this.ataque + " a ..." +otroPokemon);
    }

    Pokemon(){
        this.setPeso(1.0);
        this.setAltura(0.5);
    }

    Pokemon(String nombre, String tipo, String sonido, String ataque) {
        this.setNombre(nombre);
        this.setTipo(tipo);
        this.setSonido(sonido);
        this.setAtaque(ataque);
        this.setPeso(0.0);
        this.setAltura(0.0);
    }

    public static void main(String[] args) {
        Pokemon pokemon = new Pokemon();
        pokemon.setNombre("Pikachu");
        pokemon.setTipo("Electrico");
        pokemon.setSonido("pika pika");
        pokemon.setAtaque("Attack Trueno");

        System.out.println("Pokemon\nNombre: "+pokemon.getNombre());
        System.out.println("Tipo: "+ pokemon.getTipo());
        System.out.println("Soindo: "+ pokemon.getSonido());
        System.out.println("Ataque: "+ pokemon.getAtaque());
        System.out.println("Peso: "+ pokemon.getPeso());
        System.out.println("Altura: "+ pokemon.getAltura());

        pokemon.habla();

        pokemon.ataca("Charmander");

        System.out.println("---------------------------------------------------------");
        Pokemon pokemon1 = new Pokemon("Squirtle","Agua","Squirtle","canon de agua");

        System.out.println("Pokemon\nNombre: "+pokemon1.getNombre());
        System.out.println("Tipo: "+ pokemon1.getTipo());
        System.out.println("Soindo: "+ pokemon1.getSonido());
        System.out.println("Ataque: "+ pokemon1.getAtaque());
        System.out.println("Peso: "+ pokemon1.getPeso());
        System.out.println("Altura: "+ pokemon1.getAltura());

        pokemon1.ataca("Charmander");

    }
}
